from datetime import datetime, timedelta

from airflow import DAG
from airflow.operators.dummy_operator import DummyOperator
from airflow.utils.log.logging_mixin import LoggingMixin

log = LoggingMixin().log

try:
    # Kubernetes is optional, so not available in vanilla Airflow
    # pip install apache-airflow[kubernetes]
    from airflow.contrib.operators.kubernetes_pod_operator import KubernetesPodOperator

    default_args = {
        'owner': 'feel-the-news',
        'depends_on_past': False,
        'start_date': datetime.utcnow(),
        'schedule_interval': '@daily'
        'email': ['julia.hermann93@gmail.com'],
        'email_on_failure': True,
        'email_on_retry': False,
        'retries': 1,
        'retry_delay': timedelta(minutes=5)
    }

    dag = DAG(
        'kubernetes_sample', default_args=default_args, schedule_interval=timedelta(minutes=10))

    start = DummyOperator(task_id='run_this_first', dag=dag)

    passing = KubernetesPodOperator(namespace='default',
                                    image="Python:3.6",
                                    cmds=["Python", "-c"],
                                    arguments=["print('hello world')"],
                                    labels={"foo": "bar"},
                                    name="passing-test",
                                    task_id="passing-task",
                                    get_logs=True,
                                    dag=dag
                                    )

    failing = KubernetesPodOperator(namespace='default',
                                    image="ubuntu:1604",
                                    cmds=["Python", "-c"],
                                    arguments=["print('hello world')"],
                                    labels={"foo": "bar"},
                                    name="fail",
                                    task_id="failing-task",
                                    get_logs=True,
                                    dag=dag
                                    )

    passing.set_upstream(start)
    failing.set_upstream(start)

except ImportError as e:
    log.warn("Could not import KubernetesPodOperator: " + str(e))
    log.warn("Install kubernetes dependencies with: "
             "    pip install apache-airflow[kubernetes]")
