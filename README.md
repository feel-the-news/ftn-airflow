# Aiflow storage [![pipeline status](https://gitlab.com/feel-the-news/ftn-airflow/badges/master/pipeline.svg)](https://gitlab.com/feel-the-news/ftn-airflow/commits/master) [![coverage report](https://gitlab.com/feel-the-news/ftn-airflow/badges/master/coverage.svg)](https://gitlab.com/feel-the-news/ftn-airflow/commits/master)

This repo stores the DAGs for Apache Airflow using KubernetesPodOperator/

## Instructions to set up on Google Cloud Platform:

Requirements:
* Existing GCP project, GKE cluster
* Repository storing the DAGs needs to be publicly accessible (no SSH support yet)


1. Install AirflowOperator on the GKE cluster from the MarketPlace (default namespace, new service account)
2. Deploy AirflowBase and AirflowCluster CRD-s :
```bash
# create AirflowBase resource first
$ kubectl apply -f - <<EOF
apiVersion: airflow.k8s.io/v1alpha1
kind: AirflowBase
metadata:
  name: ftn-base
spec:
  mysql:
    operator: False
EOF

# get status of the CRs
$ kubectl get airflowbase/ftn-base -o yaml

# after 30-60s deploy cluster components
# create AirflowCluster resource next
$ kubectl apply -f - <<EOF
apiVersion: airflow.k8s.io/v1alpha1
kind: AirflowCluster
metadata:
  name: ftn-cluster
spec:
  executor: Kubernetes
  redis:
    operator: False
  scheduler:
    version: "1.10.1"
  ui:
    version: "1.10.1"
    replicas: 1
  worker:
    version: "1.10.1"
    replicas: 2
  dags:
    subdir: "dags/"
    git:
      repo: "https://gitlab.com/feel-the-news/ftn-airflow"
      once: False
  airflowbase:
    name: ftn-base
EOF

# get status of the cluster
$ kubectl get airflowcluster/ftn-cluster -o yaml

# after 30-60s deploy port forward to access UI
# access UI by pointing browser to localhost:8080
$ kubectl port-forward ftn-cluster-airflowui-0 8080:8080

# To actually see localhost just type this and click on the link
$ curl http://localhost:8080

```

## To do:
* Set up logging for tasks
* Run ftn-crawler container (uses google cloud storage)
* Use ssh private repo for DAG repo (updates in git repo are handled?)
* Check if pods are created upon running tasks

## References:
https://kubernetes.io/blog/2018/06/28/airflow-on-kubernetes-part-1-a-different-kind-of-operator/

https://github.com/helm/charts/tree/master/stable/airflow 

https://medium.com/bluecore-engineering/were-all-using-airflow-wrong-and-how-to-fix-it-a56f14cb0753 

https://github.com/puckel/docker-airflow

https://velotio.com/blog/2018/12/24/know-everything-about-spinnaker-and-how-to-deploy-using-kubernetes-engine 

https://www.youtube.com/watch?v=A0gKV1r7w8M

https://lemag.sfeir.com/installing-and-using-apache-airflow-on-the-google-cloud-platform/ 

https://schd.ws/hosted_files/kccna18/a8/Airflow%20Kubecon%20Talk%20Final.pdf 

https://www.youtube.com/watch?v=VrsVbuo4ENE&feature=youtu.be 
